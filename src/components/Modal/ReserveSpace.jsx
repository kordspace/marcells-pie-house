import React from "react";
import Datetime from "react-datetime"
import moment from 'moment'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Close from "@material-ui/icons/Close";
import Assignment from "@material-ui/icons/Assignment";
import Face from "@material-ui/icons/Face";
import Timeline from "@material-ui/icons/Timeline";
import Code from "@material-ui/icons/Code";
import Group from "@material-ui/icons/Group";
import Email from "@material-ui/icons/Email";
import Check from "@material-ui/icons/Check";
import Phone from "@material-ui/icons/Phone";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.jsx"
import InfoArea from "components/InfoArea/InfoArea.jsx";

import javascriptStyles from "assets/jss/material-kit-pro-react/views/componentsSections/javascriptStyles.jsx";

function Transition(props) {
  return <Slide direction="down" {...props} />;
}

class ReserveSpace extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signupModal: false,
      groupName: '',
      email: '',
      phone: '',
      space: '',
      dateTime: moment(),
      numberPeople: '',
      message: ''
    };
  }

  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    this.timeOutFunction = setTimeout(
      function () {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleDatetimeChange = (dateTime) => {     
    this.setState({
      dateTime: dateTime.valueOf()
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
  }

  handleClickOpen(modal) {
    var x = [];
    x[modal] = true;
    this.setState(x);
  }
  handleClose(modal) {
    var x = [];
    x[modal] = false;
    this.setState(x);
  }
  render() {
    const { classes } = this.props;
    const people = [];
    for (var i = 1; i <= 20; i++) {
      people.push(i);
    }

    return (
      <div>
        <Button color="warning" onClick={() => this.handleClickOpen("signupModal")}>
          <Assignment /> RESERVE SPACE
        </Button>
        <Dialog
          classes={{
            root: classes.modalRoot,
            paper: classes.modal + " " + classes.modalSignup
          }}
          open={this.state.signupModal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => this.handleClose("signupModal")}
          aria-labelledby="signup-modal-slide-title"
          aria-describedby="signup-modal-slide-description"
        >
          <Card plain className={classes.modalSignupCard}>
            <DialogTitle
              id="signup-modal-slide-title"
              disableTypography
              className={classes.modalHeader}
            >
              <Button
                simple
                className={classes.modalCloseButton}
                key="close"
                aria-label="Close"
                onClick={() => this.handleClose("signupModal")}
              >
                {" "}
                <Close className={classes.modalClose} />
              </Button>
              <h5 className={`${classes.cardTitle} ${classes.modalTitle}`}>
                Register
              </h5>
            </DialogTitle>
            <DialogContent
              id="signup-modal-slide-description"
              className={classes.modalBody}
            >
              <CustomInput
                labelText="Group Name"
                id="groupName"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "text",
                  onChange: (event) => this.handleChange(event),
                  endAdornment: (
                    <InputAdornment position="end">
                      <Group className={classes.inputAdornmentIcon} />
                    </InputAdornment>
                  )
                }}
              />
              <CustomInput
                labelText="Email"
                id="email"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "email",
                  onChange: (event) => this.handleChange(event),
                  endAdornment: (
                    <InputAdornment position="end">
                      <Email className={classes.inputAdornmentIcon} />
                    </InputAdornment>
                  )
                }}
              />
              <CustomInput
                labelText="Phone"
                id="phone"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "phone",
                  onChange: (event) => this.handleChange(event),
                  endAdornment: (
                    <InputAdornment position="end">
                      <Phone className={classes.inputAdornmentIcon} />
                    </InputAdornment>
                  )
                }}
              />
              <CustomDropdown
                buttonText={ "Desired Space: "+this.state.space }
                id="space"
                dropdownList={[
                  "Music Room: $50",
                  "Patio: $40",
                  "Main Hall: $30",
                  "Fire Pit: $10",
                  "Library: $5"
                ]}
                onClick={(menuItem) => {                                  
                  this.setState({
                    space: menuItem
                  })
                }}
              />
              <Datetime
                name="dateTime"
                inputProps={{ placeholder: "Select Desired Date and Time" }}
                onChange={ moment => this.handleDatetimeChange(moment)  }
              />
              <CustomDropdown
                buttonText={ "Number of People: "+this.state.numberPeople }
                id="numberPeople"
                dropdownList={people}
                onClick={(menuItem) => {                                  
                  this.setState({
                    numberPeople: menuItem
                  })
                }}
              />
              <CustomInput
                labelText="Message"
                id="message"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "text",
                  onChange: (event) => this.handleChange(event),
                  endAdornment: (
                    <InputAdornment position="end">
                      <Group className={classes.inputAdornmentIcon} />
                    </InputAdornment>
                  )
                }}
              />
            </DialogContent>
          </Card>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(javascriptStyles)(ReserveSpace);