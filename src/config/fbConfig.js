import firebase from 'firebase/app'
import 'firebase/firestore' 
import 'firebase/auth' 

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDJNM8nvip3K9Cr4YmN6GfEkJHOt9_kh84",
  authDomain: "marcells-pie-house.firebaseapp.com",
  databaseURL: "https://marcells-pie-house.firebaseio.com",
  projectId: "marcells-pie-house",
  storageBucket: "marcells-pie-house.appspot.com",
  messagingSenderId: "717105759213"
};
  firebase.initializeApp(config);
  firebase.firestore().settings({ timestampsInSnapshots: true }); 

  export default firebase;