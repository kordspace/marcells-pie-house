import React from "react";
import { compose } from "redux"
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// core components
import Header from "components/Header/Header.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
// sections of this Page
import SectionHeaders from "./Sections/SectionHeaders.jsx";
import SectionServices from "./Sections/SectionServices.jsx";
import SectionMenu from "./Sections/SectionMenu.jsx";
import SectionTeams from "./Sections/SectionTeams.jsx";
import SectionAbout from "./Sections/SectionAbout.jsx";
import SectionPricing from "./Sections/SectionPricing.jsx";
import SectionTestimonials from "./Sections/SectionTestimonials.jsx";
import SectionContacts from "./Sections/SectionContacts.jsx";
import SectionSocial from "./Sections/SectionSocial.jsx"
import sectionsPageStyle from "assets/jss/material-kit-pro-react/views/sectionsPageStyle.jsx";

class SectionsPage extends React.Component {
  componentDidMount() {
    var href = window.location.href.substring(
      window.location.href.lastIndexOf("#") + 1
    );
    if (window.location.href.lastIndexOf("#") > 0)
      document.getElementById(href).scrollIntoView();
    window.addEventListener("scroll", this.updateView);
    this.updateView();
  }
  componentDidUpdate() {
    var href = window.location.href.substring(
      window.location.href.lastIndexOf("#") + 1
    );
    if (href.includes("/")) {
      document.getElementById("home").scrollIntoView();
    } else {
      document.getElementById(href).scrollIntoView();
    }
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.updateView);
  }
  easeInOutQuad(t, b, c, d) {
    t /= d / 2;
    if (t < 1) return c / 2 * t * t + b;
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
  }
  updateView() {
    var contentSections = document.getElementsByClassName("cd-section");
    var navigationItems = document
      .getElementById("cd-vertical-nav")
      .getElementsByTagName("a");

    for (let i = 0; i < contentSections.length; i++) {
      var activeSection =
        parseInt(navigationItems[i].getAttribute("data-number"), 10) - 1;
      if (
        contentSections[i].offsetTop - window.innerHeight / 2 <
          window.pageYOffset &&
        contentSections[i].offsetTop +
          contentSections[i].scrollHeight -
          window.innerHeight / 2 >
          window.pageYOffset
      ) {
        navigationItems[activeSection].classList.add("is-selected");
      } else {
        navigationItems[activeSection].classList.remove("is-selected");
      }
    }
  }
  smoothScroll(target) {    
    var targetScroll = document.getElementById(target);
    this.scrollGo(document.documentElement, targetScroll.offsetTop, 1250);
  }
  scrollGo(element, to, duration) {
    var start = element.scrollTop,
      change = to - start,
      currentTime = 0,
      increment = 20;

    var animateScroll = function() {
      currentTime += increment;
      var val = this.easeInOutQuad(currentTime, start, change, duration);
      element.scrollTop = val;
      if (currentTime < duration) {
        setTimeout(animateScroll, increment);
      }
    }.bind(this);
    animateScroll();    
  }
  render() {
    const { classes } = this.props;
    const { hero } = this.props;
    const { about } = this.props;
    const { consumers } = this.props;
    const { suppliers } = this.props;
    const { origins } = this.props;
    const { contact } = this.props;
    const { footer } = this.props;
    const { navbar } = this.props;    
    return (
      <div>
        {/*<Header
          color="info"
          brand="Magic Agency"
          links={<HeaderLinks dropdownHoverColor="info" />}
          fixed
        />*/}
        <div className={classes.main}>
          <SectionHeaders id="home" />
          <SectionAbout id="about" />
          <SectionServices id="services" />
          <SectionMenu id="menu" />  
          <SectionTeams id="teams" />
          <SectionSocial id="social" />                                          
          <SectionContacts id="contacts" />
        </div>
        <nav id="cd-vertical-nav">
          <ul>
            <li>
              <a
                href="#home"
                data-number="1"
                className="is-selected"
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("home");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Home</span>
              </a>
            </li>            
            <li>
              <a
                href="#about"
                data-number="2"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("about");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">About</span>
              </a>
            </li>              
            <li>
              <a
                href="#services"
                data-number="3"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("services");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Services</span>
              </a>
            </li>
            <li>
              <a
                href="#menu"
                data-number="4"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("menu");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Menu</span>
              </a>
            </li>            
            <li>
              <a
                href="#teams"
                data-number="5"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("teams");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Events</span>
              </a>
            </li>
            <li>
              <a
                href="#social"
                data-number="6"
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("social");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Social</span>
              </a>
            </li>
                       
            <li>
              <a
                href="#contacts"
                data-number="7"  
                className=""
                onClick={e => {
                  var isMobile = navigator.userAgent.match(
                    /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                  );
                  if (isMobile) {
                    // if we are on mobile device the scroll into view will be managed by the browser
                  } else {
                    e.preventDefault();
                    this.smoothScroll("contacts");
                  }
                }}
              >
                <span className="cd-dot" />
                <span className="cd-label">Contact</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    hero: state.firestore.ordered.hero,
    about: state.firestore.ordered.about,
    consumers: state.firestore.ordered.consumers,
    suppliers: state.firestore.ordered.suppliers,
    origins: state.firestore.ordered.origins,
    contact: state.firestore.ordered.contact,
    footer: state.firestore.ordered.footer,
    navbar: state.firestore.ordered.navbar,
  }
}

export default compose(
  connect(mapStateToProps),
  withStyles(sectionsPageStyle),
  firestoreConnect([
    {
      collection: 'hero',
      orderBy: 'order'
    },
    {
      collection: 'key'
    },
    {
      collection: 'about',
      orderBy: 'order'
    },
    {
      collection: 'consumers',
      orderBy: 'order'
    },
    {
      collection: 'suppliers',
      orderBy: 'order'
    },
    {
      collection: 'origins',
      orderBy: 'order'
    },
    {
      collection: 'contact',
      orderBy: 'order'
    },
    {
      collection: 'footer',
      orderBy: 'order'
    },
    {
      collection: 'navbar',
      orderBy: 'order'
    }
  ])
)(SectionsPage)
