import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import officeStyle from "assets/jss/material-kit-pro-react/views/aboutUsSections/officeStyle.jsx";

// office
import core1 from "assets/img/examples/core1.png";
import core2 from "assets/img/examples/core2.jpeg";
import core3 from "assets/img/examples/core3.png";
import core4 from "assets/img/examples/core4.png";
import core5 from "assets/img/examples/core5.png";

function SectionOffice(props) {
  const { classes } = props;
  return (
    <div className={classes.office}>
      <GridContainer className={classes.textCenter}>
        <GridItem
          md={8}
          sm={8}
          className={classNames(classes.mrAuto, classes.mlAuto)}
        >
          <h1 className={classes.title}>Our Core Values</h1>
          <h4 className={classes.description}>
          </h4>
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem md={4} sm={4}>
          <img
            className={classNames(
              classes.imgRaised,
              classes.imgFluid,
              classes.rounded
            )}
            src={core1}
            alt="core1"
          />
          <h3 className={classes.title}>BEING REAL</h3>
          <h4 className={classes.description}>We value transparency, authenticity, and the strength of our relationships above all else.</h4>
        </GridItem>
        <GridItem md={4} sm={4}>
          <img
            className={classNames(
              classes.imgRaised,
              classes.imgFluid,
              classes.rounded
            )}
            src={core2}
            alt="core2"
          />
          <h3 className={classes.title}>EXTREME OWNERSHIP</h3>
          <h4 className={classes.description}>We value transparency, authenticity, and the strength of our relationships above all else.</h4>
        </GridItem>
        <GridItem md={4} sm={4}>
          <img
            className={classNames(
              classes.imgRaised,
              classes.imgFluid,
              classes.rounded
            )}
            src={core3}
            alt="core3"
          />
          <h3 className={classes.title}>GETTING WEIRD</h3>
          <h4 className={classes.description}>We don’t take ourselves too seriously.</h4>
        </GridItem>
        <GridItem md={6} sm={6}>
          <img
            className={classNames(
              classes.imgRaised,
              classes.imgFluid,
              classes.rounded
            )}
            src={core4}
            alt="core4"
          />
          <h3 className={classes.title}>PERSONAL GROWTH</h3>
          <h4 className={classes.description}>A foundation in personal and professional development helps us scale the missions of impactful organizations.</h4>
        </GridItem>
        <GridItem md={6} sm={6}>
          <img
            className={classNames(
              classes.imgRaised,
              classes.imgFluid,
              classes.rounded
            )}
            src={core5}
            alt="core5"
          />
          <h3 className={classes.title}>CONSTANT LEARNING</h3>
          <h4 className={classes.description}>We thirst for new opportunities to learn because we know that learners are leaders.</h4>
        </GridItem>
      </GridContainer>
    </div>
  );
}

export default withStyles(officeStyle)(SectionOffice);
