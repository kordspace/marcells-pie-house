import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import Share from "@material-ui/icons/Share";
import ChatBubble from "@material-ui/icons/ChatBubble";
import Schedule from "@material-ui/icons/Schedule";
import TrendingUp from "@material-ui/icons/TrendingUp";
import Subject from "@material-ui/icons/Subject";
import WatchLater from "@material-ui/icons/WatchLater";
import People from "@material-ui/icons/People";
import Business from "@material-ui/icons/Business";
import Check from "@material-ui/icons/Check";
import Close from "@material-ui/icons/Close";
import Delete from "@material-ui/icons/Delete";
import Bookmark from "@material-ui/icons/Bookmark";
import Refresh from "@material-ui/icons/Refresh";
import Receipt from "@material-ui/icons/Receipt";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Form from "components/Forms/Form1.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import Info from "components/Typography/Info.jsx";
import Danger from "components/Typography/Danger.jsx";
import Success from "components/Typography/Success.jsx";
import Warning from "components/Typography/Warning.jsx";
import Rose from "components/Typography/Rose.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import styles from "assets/jss/material-kit-pro-react/views/homeSections/sectionForm.jsx";

import cardBlog1 from "assets/img/examples/card-blog1.jpg";
import cardBlog2 from "assets/img/examples/card-blog2.jpg";
import cardBlog3 from "assets/img/examples/card-blog3.jpg";
import cardBlog5 from "assets/img/examples/card-blog5.jpg";
import cardBlog6 from "assets/img/examples/card-blog6.jpg";
import cardProfile1 from "assets/img/examples/card-profile1.jpg";
import cardProfile4 from "assets/img/examples/card-profile4.jpg";
import blog1 from "assets/img/examples/blog1.jpg";
import blog5 from "assets/img/examples/blog5.jpg";
import blog6 from "assets/img/examples/blog6.jpg";
import blog8 from "assets/img/examples/blog8.jpg";
import avatar from "assets/img/faces/avatar.jpg";
import christian from "assets/img/faces/christian.jpg";
import marc from "assets/img/faces/marc.jpg";
import office1 from "assets/img/examples/office1.jpg";
import color1 from "assets/img/examples/color1.jpg";
import color2 from "assets/img/examples/color2.jpg";
import color3 from "assets/img/examples/color3.jpg";
import card1 from "assets/img/about/card1.jpg";
import card2 from "assets/img/about/card2.jpg";
import card3 from "assets/img/about/card3.jpg";
import instagram from "assets/img/instagram.jpg";
import facebook from "assets/img/facebook.jpg";
import linkedin from "assets/img/linkedin.jpg";
import contact from "assets/img/contact.jpg";
import footercard from "assets/img/form/card1.jpg";

import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

const CustomSkinMap = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={7}
      defaultCenter={{ lat: 33.679227, lng: -117.911355 }}
      defaultOptions={{
        scrollwheel: false,
        zoomControl: true,
        styles: [
          {
            featureType: "water",
            stylers: [
              { saturation: -40 },
              { lightness: -35 },
              { hue: "#3A7AB7" }
            ]
          },
          {
            featureType: "road",
            elementType: "geometry.fill",
            stylers: [
              { hue: "#ff0000" },
              { saturation: -100 },
              { lightness: 99 }
            ]
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#808080" }, { lightness: 54 }]
          },
          {
            featureType: "landscape.man_made",
            elementType: "geometry.fill",
            stylers: [{ color: "#F7F7F7" }]
          },
          {
            featureType: "poi.park",
            elementType: "geometry.fill",
            stylers: [{ color: "#BACE3F" }]
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#3A7AB7" }]
          },
          {
            featureType: "road",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#ffffff" }]
          },
          { featureType: "poi", stylers: [{ visibility: "off" }] },
          {
            featureType: "landscape.natural",
            elementType: "geometry.fill",
            stylers: [{ visibility: "on" }, { color: "#BACE3F" }]
          },
          { featureType: "poi.park", stylers: [{ visibility: "on" }] },
          {
            featureType: "poi.sports_complex",
            stylers: [{ visibility: "off" }]
          },
          { featureType: "poi.medical", stylers: [{ visibility: "off" }] },
          {
            featureType: "poi.business",
            stylers: [{ visibility: "on" }]
          }
        ]
      }}
    >
      <Marker position={{ lat: 33.679227, lng: -117.911355 }} />
    </GoogleMap>
  ))
);


class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRotate1: "",
      activeRotate2: ""
    };
  }
  componentDidMount() {
    const { classes } = this.props;
    var rotatingCards = document.getElementsByClassName(classes.cardRotate);
    for (let i = 0; i < rotatingCards.length; i++) {
      var rotatingCard = rotatingCards[i];
      var rotatingWrapper = rotatingCard.parentElement;
      var cardWidth = rotatingCard.parentElement.offsetWidth;
      var cardHeight = rotatingCard.children[0].children[0].offsetHeight;
      rotatingWrapper.style.height = cardHeight + "px";
      rotatingWrapper.style["margin-bottom"] = 30 + "px";
      var cardFront = rotatingCard.getElementsByClassName(classes.front)[0];
      var cardBack = rotatingCard.getElementsByClassName(classes.back)[0];
      cardFront.style.height = cardHeight + 35 + "px";
      cardFront.style.width = cardWidth + "px";
      cardBack.style.height = cardHeight + 35 + "px";
      cardBack.style.width = cardWidth + "px";
    }
  }
  render() {
    const { classes, ...rest } = this.props;
    const { contact } = this.props;
    if (contact) {
      return (
        <div {...rest} className="cd-section" id="footer">
          <div className={classes.sectionGray}>
            <div>
              {/* DYNAMIC COLORED SHADOWS START */}
              <div className={classes.container}>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <div className={classes.title}>
                      <h2 className="center">{contact[0].content}
                      </h2>
                      <h3 className="center">{contact[1].content}
                      </h3>
                      <h5 className="center">
                        {contact[2].content}
                      </h5>
                      <h4 className="center">
                        {contact[3].content}
                      </h4>
                    </div>
                  </GridItem>
                </GridContainer>
                <GridContainer className={classes.gridContainer}>
                  <GridItem xs={12} sm={12} md={5}>
                    <Card blog className={classes.cardColor}>
                      <CardHeader image>
                        <img src={footercard} alt="large satellite tower" />
                        <div
                          className={classes.coloredShadow}
                          style={{
                            backgroundImage: `url(${footercard})`,
                            opacity: "1"
                          }}
                        />
                      </CardHeader>
                      <CardBody>
                        {/*<Warning>
                        <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                        </Warning>*/}
                        <h3 className={classes.aboutCardDescription}>
                          {contact[4].content}<br></br>
                        </h3>
                        <h4 className={classes.aboutPrivacyDescription}>
                          Phone: <a href="tel:6572189210">{contact[5].content}</a><br></br>
                          Email: <a href="mailto:info@endofwaste.com">{contact[6].content}</a><br></br>
                          Address: {contact[7].content}
                        </h4>
                        <div className={classes.bigMap}>
                          <CustomSkinMap
                            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8VJqkT2mCGDtuIy_3UPm0qIiLWxyoZG4"
                            loadingElement={<div style={{ height: `100%` }} />}
                            containerElement={
                              <div
                                style={{
                                  height: `100%`,
                                  borderRadius: "6px",
                                  overflow: "hidden"
                                }}
                              />
                            }
                            mapElement={<div style={{ height: `100%` }} />}
                          />
                        </div>
                      </CardBody>
                    </Card>
                    {/*<Card className={classes.cardColor}>
                      <CardBody>
                        <Warning>
                            <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                            </Warning>
                        <h3 className={classes.aboutCardDescription}>
                          {contact[8].content}<br></br>
                        </h3>
                        <h4 className={classes.aboutPrivacyDescription}>
                          {contact[9].content}
                        </h4>
                      </CardBody>
                    </Card>*/}
                  </GridItem>
                  <GridItem xs={12} sm={12} md={7}>
                    <GridContainer>
                      <Card blog className={classes.cardColor}>
                        <CardBody>
                          {/*<Warning>
                        <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                        </Warning>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                              <div className={classes.form}>
                                <h2 className="center">{contact[11].content}
                                </h2>
                              </div>
                            </GridItem>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                id="not-logged-name"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  placeholder: "Your First Name"
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                id="not-logged-email"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  placeholder: "Your Last Name"
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                id="not-logged-email"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  placeholder: "Your Email"
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                id="not-logged-email"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  placeholder: "Your Phone Number"
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                id="not-logged-email"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  placeholder: "Your Company Name"
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                id="not-logged-email"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  placeholder: "Your Company Type"
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                id="not-logged-email"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  placeholder: "Your Comments"
                                }}
                              />
                            </GridItem>
                            <Button
                              color="info"
                              className={classes.navButton}
                              round
                            >
                              {contact[10].content}
                              </Button>
                          </GridContainer>*/}
                          <div>
                            {/* Begin Mailchimp Signup Form */}
                            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css" />
                            <style type="text/css" dangerouslySetInnerHTML={{ __html: "\n\t#mc_embed_signup{background:#fafafa; clear:left; font:14px Helvetica,Arial,sans-serif; }\n\t/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.\n\t   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */\n" }} />
                            <div id="mc_embed_signup">
                              <form action="https://endofwaste.us19.list-manage.com/subscribe/post?u=ba44c2bfd89ab186b45e29b54&id=4884316e91" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
                                <div className={classes.contactHeader} id="mc_embed_signup_scroll">
                                  <p>Contact Us</p> 
                                  <div className="mc-field-group">
                                    <label htmlFor="mce-FNAME">First Name </label>
                                    <input type="text" name="FNAME" className id="mce-FNAME" />
                                  </div>
                                  <div className="mc-field-group">
                                    <label htmlFor="mce-LNAME">Last Name </label>
                                    <input type="text" name="LNAME" className id="mce-LNAME" />
                                  </div>
                                  <div className="mc-field-group">
                                    <label htmlFor="mce-EMAIL">Email Address  <span className="asterisk">*</span>
                                    </label>
                                    <input type="email" name="EMAIL" className="required email" id="mce-EMAIL" />
                                  </div>
                                  <div className="mc-field-group size1of2">
                                    <label htmlFor="mce-PHONE">Phone Number </label>
                                    <input type="text" name="PHONE" className id="mce-PHONE" />
                                  </div>
                                  <div className="mc-field-group">
                                    <label htmlFor="mce-MMERGE5">Comments </label>
                                    <input type="text" name="MMERGE5" className id="mce-MMERGE5" />
                                  </div>
                                  <div className="mc-field-group">
                                    <label htmlFor="mce-MMERGE3">Company Name </label>
                                    <input type="text" name="MMERGE3" className id="mce-MMERGE3" />
                                  </div>
                                  <div className="mc-field-group input-group">
                                    <strong>Company Type </strong>
                                    <ul><li><input type="checkbox" defaultValue={1} name="group[2743][1]" id="mce-group[2743]-2743-0" /><label htmlFor="mce-group[2743]-2743-0">Waste Management</label></li>
                                      <li><input type="checkbox" defaultValue={2} name="group[2743][2]" id="mce-group[2743]-2743-1" /><label htmlFor="mce-group[2743]-2743-1">Recycling Industry,</label></li>
                                      <li><input type="checkbox" defaultValue={4} name="group[2743][4]" id="mce-group[2743]-2743-2" /><label htmlFor="mce-group[2743]-2743-2">Product Manufacturer,</label></li>
                                      <li><input type="checkbox" defaultValue={8} name="group[2743][8]" id="mce-group[2743]-2743-3" /><label htmlFor="mce-group[2743]-2743-3">Consumer</label></li>
                                    </ul>
                                  </div>
                                  <div id="mce-responses" className="clear">
                                    <div className="response" id="mce-error-response" style={{ display: 'none' }} />
                                    <div className="response" id="mce-success-response" style={{ display: 'none' }} />
                                  </div>    {/* real people should not fill this in and expect good things - do not remove this or risk form bot signups*/}
                                  <div style={{ position: 'absolute', left: '-5000px' }} aria-hidden="true"><input type="text" name="b_ba44c2bfd89ab186b45e29b54_4884316e91" tabIndex={-1} defaultValue /></div>
                                  <div className="clear"><input type="submit" defaultValue="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="button" /></div>
                                </div>
                              </form>
                            </div>
                            {/*End mc_embed_signup*/}
                          </div>
                        </CardBody>
                      </Card>
                    </GridContainer>
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12} className={classes.cardPrivacy}>
                    <Card className={classes.cardColor} >
                      <CardBody>
                        {/*<Warning>
                            <h6 className={classes.cardCategory}>DYNAMIC SHADOWS</h6>
                            </Warning>*/}
                        <h3 className={classes.aboutCardDescription}>
                          {contact[8].content}<br></br>
                        </h3>
                        <h4 className={classes.aboutPrivacyDescription}>
                          {contact[9].content}
                        </h4>
                      </CardBody>
                    </Card>
                  </GridItem>
                </GridContainer>
              </div>
              {/* DYNAMIC COLORED SHADOWS END */}
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div></div>
      )
    }
  }
}

export default withStyles(styles)(Footer);
