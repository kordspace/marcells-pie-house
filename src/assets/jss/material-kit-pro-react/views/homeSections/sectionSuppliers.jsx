import {
    container,
    coloredShadow,
    title,
    cardTitle,
    description,
    mlAuto,
    infoColor,
    roseColor
  } from "assets/jss/material-kit-pro-react.jsx";
  
  import imageStyles from "assets/jss/material-kit-pro-react/imagesStyles.jsx";
  
  import rotatingCards from "assets/jss/material-kit-pro-react/rotatingCards.jsx";
  
  const styles = {
    container,
    coloredShadow,
    title,
    mlAuto,
    cardTitle,
    ...imageStyles,
    ...rotatingCards,
    sectionGray: {
      background: "#E5E5E5"
    },
    cardColor: {
      backgroundColor: "#fafafa"
    },
    title: {
      marginTop: "100px",
      color: "#3A7AB7",
      '@media (max-width: 525px)': {
        "& h4": {
          marginLeft: "1em",
          marginRight: "1rem",
        }
      },
      "& h3": {
        color: "#BACE3F",
      },  
      "& h2": {
        paddingTop: "150px",
        marginBottom: "20px",
        color: "#3A7AB7",
        fontSize: "3rem",
        lineHeight: "3rem",
        '@media (max-width: 525px)': {
          fontSize: "2em",
          lineHeight: "2rem",
        },
        '@media (min-width: 1000px)': {
          fontSize: "4rem",
          lineHeight: "4rem",
        },
        '@media (min-width: 2000px)': {
          fontSize: "5rem",
          lineHeight: "5rem",
        },
      },
      "& h3": {
        fontSize: "2.2rem",
        lineHeight: "2.5rem",
        color: "#BACE3F",
        '@media (max-width: 525px)': {
          fontSize: "1.5em",
          lineHeight: "1.5rem",
        },
        '@media (min-width: 1000px)': {
          fontSize: "3rem",
          lineHeight: "3rem",
        },
        '@media (min-width: 2000px)': {
          fontSize: "4rem",
          lineHeight: "4rem",
        },
  
      },
      "& h4": {
        color: "#000",
        fontFamily: "Noto Serif",
        fontSize: "1.3rem",
        lineHeight: "2rem",
        zIndex: "0",
        '@media (max-width: 525px)': {
          fontSize: "1rem",
          marginTop: "0rem",
          lineHeight: "1.5rem",
        },
        '@media (min-width: 1000px)': {
          fontSize: "2rem",
          lineHeight: "2.5rem",
        },
        '@media (min-width: 2000px)': {
          fontSize: "2.5rem",
          lineHeight: "3rem",
        },
      },
  
    },
    container: {
      marginBottom: "-50px",
      marginLeft: "4rem",
      marginRight: "4rem",
      '@media (max-width: 525px)': {
        marginLeft: "1rem",
        marginRight: "1rem",
        },
      "& h2": {
        marginLeft: "1rem",
        marginRight: "1rem",
      },  
      "& h4": {
        fontFamily: "Noto Serif",
        marginLeft: "2rem",
        marginRight: "2rem",
        color: "black"
      }
    },
    consumersForms: {
      "& h2": {
        color: "#3A7AB7",
        '@media (min-width: 1000px)': {
          fontSize: "2.5rem",
          lineHeight: "2.5rem",
        },
        '@media (min-width: 2000px)': {
          fontSize: "3rem",
          lineHeight: "3rem",
        },
  
      },
  
    },
    suppliersContainer: {
      "& h4": {
        fontFamily: "Noto Serif",
        marginLeft: "0rem",
        marginRight: "0rem",
        fontSize: "1rem",
        align: "center"
      }  
    },
    aboutSignUp: {
      minHeight: "288px"
    },
    navButton: {
      marginTop: "20px",
      backgroundColor: "rgba(57, 165, 133, 1)",
      textAlign: "center",
      float: "right"
    },
    navButtonForm: {
      marginTop: "20px",
      backgroundColor: "rgba(57, 165, 133, 1)",
      textAlign: "center",
      float: "left"
    },
    consumersFormButton: {
      background: "none !important",
      margin: "0em 0 0em 0% !important",
      border: "0px solid #ABB0B2 !important",
      fontSize: "1rem !important",
      color: "white !important",
      '@media (min-width: 1000px)': {
        fontSize: "2rem !important",
        lineHeight: "2rem !important",
      },
      '@media (min-width: 2000px)': {
        fontSize: "2.5rem !important",
        lineHeight: "2.5rem !important",
      },
    },
    sectionGray: {
      marginTop: "0px"
    },
    sectionWhite: {
      background: "#FFFFFF"
    },
    aboutSignUpTitle: {
      textAlign: "left",
      marginTop: "40px",
      color: "#3A7AB7",
      '@media (max-width: 525px)': {
        fontSize: "1.5em",
        lineHeight: "2rem",
      },
      '@media (min-width: 1000px)': {
        fontSize: "3rem",
        lineHeight: "4rem",
      },
      '@media (min-width: 2000px)': {
        fontSize: "4rem",
        lineHeight: "4rem",
      },

    },
    aboutSignUpDescription: {
      textAlign: "left",
      fontFamily: "Noto Serif",
      '@media (max-width: 525px)': {
        fontSize: "1.5em",
        lineHeight: "2rem",
      },
      '@media (min-width: 1000px)': {
        fontSize: "2rem",
        lineHeight: "2.5rem",
      },
      '@media (min-width: 2000px)': {
        fontSize: "2.5rem",
        lineHeight: "3rem",
      },

    },
    cardTitleAbsolute: {
      ...cardTitle,
      position: "absolute !important",
      bottom: "15px !important",
      left: "15px !important",
      color: "#fff !important",
      fontSize: "1.125rem !important",
      textShadow: "0 2px 5px rgba(33, 33, 33, 0.5) !important"
    },
    cardTitleWhite: {
      "&, & a": {
        ...title,
        marginTop: ".625rem",
        marginBottom: "0",
        minHeight: "auto",
        color: "#fff !important"
      }
    },
    cardCategory: {
      marginTop: "10px",
      "& svg": {
        position: "relative",
        top: "8px"
      }
    },
    cardCategorySocial: {
      marginTop: "10px",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        fontSize: "22px",
        position: "relative",
        marginTop: "-4px",
        top: "2px",
        marginRight: "5px"
      },
      "& svg": {
        position: "relative",
        top: "5px"
      }
    },
    cardCategorySocialWhite: {
      marginTop: "10px",
      color: "rgba(255, 255, 255, 0.8)",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        fontSize: "22px",
        position: "relative",
        marginTop: "-4px",
        top: "2px",
        marginRight: "5px"
      },
      "& svg": {
        position: "relative",
        top: "5px"
      }
    },
    cardCategoryWhite: {
      marginTop: "10px",
      color: "rgba(255, 255, 255, 0.7)"
    },
    cardCategoryFullWhite: {
      marginTop: "10px",
      color: "#FFFFFF"
    },
    cardDescription: {
      ...description
    },
    cardDescriptionWhite: {
      color: "rgba(255, 255, 255, 0.8)"
    },
    aboutCardDescription: {
      color: "#000000 !important"
    },
    author: {
      display: "inline-flex",
      "& a": {
        color: "#3C4858"
      }
    },
    authorWhite: {
      display: "inline-flex",
      "& a": {
        color: "rgba(255, 255, 255, 0.8)"
      }
    },
    avatar: {
      width: "30px",
      height: "30px",
      overflow: "hidden",
      borderRadius: "50%",
      marginRight: "5px"
    },
    statsWhite: {
      color: "rgba(255, 255, 255, 0.8)",
      display: "inline-flex",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        fontSize: "18px",
        lineHeight: "18px"
      },
      "& svg": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        width: "18px",
        height: "18px"
      }
    },
    stats: {
      color: "#999999",
      display: "flex",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        fontSize: "18px",
        lineHeight: "18px"
      },
      "& svg": {
        position: "relative",
        top: "3px",
        marginRight: "3px",
        marginLeft: "3px",
        width: "18px",
        height: "18px"
      }
    },
    justifyContentCenter: {
      WebkitBoxPack: "center !important",
      MsFlexPack: "center !important",
      justifyContent: "center !important"
    },
    iconWrapper: {
      color: "rgba(255, 255, 255, 0.76)",
      margin: "10px auto 0",
      width: "130px",
      height: "130px",
      border: "1px solid #E5E5E5",
      borderRadius: "50%",
      lineHeight: "174px",
      "& .fab,& .fas,& .far,& .fal,& .material-icons": {
        fontSize: "55px",
        lineHeight: "55px"
      },
      "& svg": {
        width: "55px",
        height: "55px"
      }
    },
    iconWrapperColor: {
      borderColor: "rgba(255, 255, 255, 0.25)"
    },
    iconWhite: {
      color: "#FFFFFF"
    },
    iconRose: {
      color: roseColor
    },
    iconInfo: {
      color: infoColor
    },
    marginTop30: {
      marginTop: "30px"
    },
    textCenter: {
      textAlign: "center"
    },
    marginBottom20: {
      marginBottom: "20px"
    }
  };
  
  export default styles;
  