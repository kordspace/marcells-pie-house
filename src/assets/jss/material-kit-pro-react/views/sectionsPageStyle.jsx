import { main } from "assets/jss/material-kit-pro-react.jsx";

const sectionsPageStyle = {
  main: {
    ...main,
    marginTop: "-70px"
  }
};

export default sectionsPageStyle;
