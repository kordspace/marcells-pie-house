import authReducer from './authReducer'
import formReducer from './formReducer'
import contentReducer from './contentReducer'
import { combineReducers } from 'redux'
import { firestoreReducer } from 'redux-firestore'

const rootReducer = combineReducers({
    auth: authReducer,
    form: formReducer,
    content: contentReducer,
    firestore: firestoreReducer
});

export default rootReducer